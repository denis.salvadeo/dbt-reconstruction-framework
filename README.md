# DBT Reconstruction Framework

This software performs 3-D tomographic reconstruction from a set of 2-D projections of Digital Breast Tomosynthesis (DBT).

This software is being developed mainly by Prof. Dr. Denis Henrique Pinheiro Salvadeo (UNESP - São Paulo State University, Rio Claro/SP, Brazil) in collaboration with Prof. Dr. Andrew Douglas Arnold Maidment (University of Pennsylvania, Philadelphia/PA, USA).

So far, the following reconstruction algorithms is implemented in this software:
- Backprojection;
- Filtered Backprojection (FBP);
- Simultaneous Algebraic Reconstruction Technique (SART);
- Maximum Lihelihood-Expectation Maximization (MLEM);
- Maximum *a Posteriori* (MAP) with the following *a priori* probabilities:
    * Gaussian Markov Random Field (GMRF);
    * Non Local Gaussian Markov Random Field (NLGMRF);
    * Sparse Non Local Gaussian Markov Random Field (SNLGMRF) - to be published by the authors.

Currently, the source code files are being prepared to become available for the public with in this repository. However, while it is not yet available here, if you are interested in this software, please send a message to Prof. Denis Salvadeo (denis.salvadeo@unesp.br).